#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "../src/vector.h"

TEST_CASE("Vector<int>Test, CopyConstructor") {
  const Vector<int> v({0,1,2,3,4,5,6,7,8,9});
  Vector<int> other{v};

  CHECK( other.size() == 10 );
  CHECK( other.empty() == false );

  for (size_t i=0;i<other.size();i++) {
    CHECK(v[i] == other[i]);
  }

  other.pop_back();

  CHECK( v.size() == other.size() + 1 );
}

TEST_CASE("Vector<int>Test, Assignment") {
  const Vector<int> a({0,1,2,3,4,5,6,7,8,9});
  Vector<int> b;
  b = a;

  for(size_t i=0; i < a.size(); ++i)
    CHECK(a[i] == b[i]);

  CHECK(a.empty() == false);
  CHECK(a.size() == 10);
  CHECK(b.size() == 10);

  b.pop_back();

  CHECK(a.size() == 10);
  CHECK(b.size() == 9);
}

TEST_CASE("IteratorTest, DefaultConstructible") {
  Vector<int>::iterator a;
  Vector<int>::const_iterator b;
}

TEST_CASE("IteratorTest, CopyConstructible") {
  Vector<int> v({1,2});
  Vector<int>::iterator a{v.begin()};
  Vector<int>::iterator b{a};
  CHECK_EQ(a, b);
}

TEST_CASE("IteratorTest, CopyAssignable") {
  Vector<int> v({1,2});
  Vector<int>::iterator a{v.begin()};
  Vector<int>::iterator b{v.end()};
  CHECK_NE(a, b);
  a = b;
  CHECK_EQ(a, b);
}

TEST_CASE("IteratorTest, PreIncrement") {
  Vector<int> v({1});
  Vector<int>::iterator a{v.begin()};
  Vector<int>::iterator b{v.end()};
  CHECK_NE(a, b);
  CHECK_EQ(++a, b);
}

TEST_CASE("IteratorTest, PostIncrement") {
  Vector<int> v({1});
  Vector<int>::iterator a{v.begin()};
  Vector<int>::iterator b{v.end()};
  CHECK_NE(a, b);
  CHECK_NE(a++, b);
  CHECK_EQ(a, b);
}

TEST_CASE("IteratorTest, Dereference") {
  Vector<int> v({0,1,2,3,4});
  {
    size_t cnt = 0;
    for(auto it = v.begin(); it != v.end(); ++it) {
      CHECK_EQ(*it, cnt++);
    }
    CHECK_EQ(cnt, 5);
  }
  auto it = v.begin();
  CHECK_EQ(*it, 0);
  *it = 999;
  CHECK_EQ(*it, 999);
}

TEST_CASE("IteratorTest, ArrowOperator") {
  Vector<int> v({0,1,2,3,4});
  Vector<int>::iterator a{v.begin()};
  Vector<int>::iterator b{v.end()};
  {
    size_t cnt = 0;
    for(auto it = v.begin(); it != v.end(); ++it) {
      CHECK_EQ(*(it.operator->()), cnt++);
    }
    CHECK_EQ(cnt, 5);
  }
}

/**
 * ConstIterator
 */
TEST_CASE("ConstIteratorTest, CopyConstructible") {
  Vector<int> v({1,2});
  Vector<int>::const_iterator a{v.begin()};
  Vector<int>::const_iterator b{a};
  CHECK_EQ(a, b);
}

TEST_CASE("ConstIteratorTest, CopyAssignable") {
  Vector<int> v({1,2});
  Vector<int>::const_iterator a{v.begin()};
  Vector<int>::const_iterator b{v.end()};
  CHECK_NE(a, b);
  a = b;
  CHECK_EQ(a, b);
}

TEST_CASE("ConstIteratorTest, PreIncrement") {
  Vector<int> v({1});
  Vector<int>::const_iterator a{v.begin()};
  Vector<int>::const_iterator b{v.end()};
  CHECK_NE(a, b);
  CHECK_EQ(++a, b);
}

TEST_CASE("ConstIteratorTest, PostIncrement") {
  Vector<int> v({1});
  Vector<int>::const_iterator a{v.begin()};
  Vector<int>::const_iterator b{v.end()};
  CHECK_NE(a, b);
  CHECK_NE(a++, b);
  CHECK_EQ(a, b);
}

TEST_CASE("ConstIteratorTest, Dereference") {
  Vector<int> v({0,1,2,3,4});
  Vector<int>::const_iterator beg{v.begin()};
  Vector<int>::const_iterator end{v.end()};
  {
    size_t cnt = 0;
    for(; beg != end; ++beg) {
      CHECK_EQ(*beg, cnt++);
    }
    CHECK_EQ(cnt, 5);
  }
}

TEST_CASE("ConstIteratorTest, ArrowOperator") {
  Vector<int> v({0,1,2,3,4});
  Vector<int>::const_iterator beg{v.begin()};
  Vector<int>::const_iterator end{v.end()};
  {
    size_t cnt = 0;
    for(; beg != end; ++beg) {
      CHECK_EQ(*(beg.operator->()), cnt++);
    }
    CHECK_EQ(cnt, 5);
  }
}

TEST_CASE("IteratorTest, TypeConversion") {
  Vector<int> v({1});
  Vector<int>::iterator it{v.begin()};
  Vector<int>::const_iterator cit{it};

  //call Vector<int>::const_iterator's operators
  CHECK(!(cit != it));
  CHECK(  cit == it);

  //call Vector<int>::iterator's operators
  CHECK(!(it != cit));
  CHECK(  it == cit);
}

TEST_CASE("IteratorTest, Insert/Erase") {
  std::cout << "here -1" << std::endl;
  Vector<int> v{1,2,3,4,5,6,7};
  std::cout << "here 0" << std::endl;
  auto x = v.insert(++(v.begin()),8);
  std::cout << "here 1" << std::endl;
  CHECK_EQ(v[1],8);
  std::cout << "here 2" << std::endl;
  CHECK_EQ(*x,8);
  std::cout << "here 3" << std::endl;
  auto it =v.begin();
  std::cout << "here 4" << std::endl;
  it++;
  it++;
  std::cout << "here 4.5" << std::endl;
  auto y = v.erase(it);
  std::cout << "here 5" << std::endl;
  CHECK_EQ(*y,3);
  std::cout << "here 6" << std::endl;
  CHECK_EQ(v[2],3);
  std::cout << "here 7" << std::endl;
}
