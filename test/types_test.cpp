#include<iostream>
#include"../src/vector.h"
using namespace std;

int main() {

  [[maybe_unused]] Vector<int>::value_type val;
  [[maybe_unused]] Vector<int>::size_type size;
  [[maybe_unused]] Vector<int>::difference_type diff;
  [[maybe_unused]] Vector<int>::reference ref = val;
  [[maybe_unused]] Vector<int>::const_reference cref = val;
  [[maybe_unused]] Vector<int>::pointer ptr = &val;
  [[maybe_unused]] Vector<int>::const_pointer cptr = &val;
  [[maybe_unused]] Vector<int>::iterator *it;
  [[maybe_unused]] Vector<int>::const_iterator *cit;

  return 0;
}
