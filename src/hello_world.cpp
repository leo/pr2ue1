#include <iostream>
#include "vector.h"
#include "nba_player.h"

using namespace::std;

typedef struct location {
    long lon;
    long lat;
} Location;

int main() {
   cout << "Hello World\n";
   
   char foo[5] = {'a', 'b', 'c', 'd', '\0'};
   
   *(foo + 1) = 'q';
   
   cout << foo;
   
   cout << "\n";
   
   cout << "sizeof(void *): " << sizeof(void *) << "\n";

   cout << "sizeof(int): " << sizeof(int) << "\n";

   Vector<int> myVector(2);

   cout << "size: " << myVector.capacity() << "\n";

   myVector.push_back(5);
   myVector.push_back(6);
   myVector.push_back(7);
   myVector.push_back(8);

   void* myMem = malloc(0);
   free(myMem);

   myVector.print();
   
   cout << "size: " << myVector.capacity() << "\n";

   cout << "elem: " << myVector.pop_back() << "\n";
   
   
   cout << "Beginning with location vector!\n";

   Vector<Location> my2Vector = Vector<Location>(2);
   
   Location wien = {1, 2};
   Location graz = {3, 4};
   Location berg = {5, 6};
   Location tal = {7, 8};
   
   my2Vector.push_back(wien);
   my2Vector.push_back(graz);
   my2Vector.push_back(berg);
   my2Vector.push_back(tal);
   
   cout << "size: " << my2Vector.capacity() << "\n";

   cout << "elem: " << my2Vector.pop_back().lat << "\n";
   
   cout << "creating nba vector\n";
   
   Vector<NBA_Player> players = Vector<NBA_Player>();
   
   cout << "adding 1 nba player\n";
   players.push_back(NBA_Player());
   cout << "adding 2 nba player\n";
   players.push_back(NBA_Player());
   cout << "adding 3 nba player\n";
   players.push_back(NBA_Player());
   cout << "adding 4 nba player\n";
   players.push_back(NBA_Player());
   cout << "adding 5 nba player\n";
   players.push_back(NBA_Player());
   cout << "adding 6 nba player\n";
   players.push_back(NBA_Player());
   cout << "adding 7 nba player\n";
   players.push_back(NBA_Player());
   cout << "all nba players added!\n";
   
   myVector.print();

   cout << "exit\n";
  
   
   
   return 0;
}
