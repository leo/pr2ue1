#ifndef NBA_PLAYER_H
#define NBA_PLAYER_H

#include"vector.h"
#include<iostream>
#include<fstream>
#include<unordered_map>
#include<map>
#include<stdexcept>
#include<algorithm>
#include<numeric>
#include<iterator>
#include<set>
#include<vector>

using namespace std;

class NBA_Player{
  string name;
  string team;
  int height;
  vector<double> stats;  // PPG last seasons
  int foo[999];

  public:
    NBA_Player(){
        this->name = "Blake Griffin";
        this->team = "Detroit_Pistons";
        this->height = 123;
        this->stats = {19.8,6.6,6.2};
    }
    string getName() const{
        return this->name;
    }
};


#endif // NBA_PLAYER_H

