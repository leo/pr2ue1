#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <cstring>
#include <stdio.h>

#ifndef VECTOR_H
#define VECTOR_H

#define INIT_SIZE 16

using namespace std;

template <typename T> class Vector {
    public:
    
        class Iterator;
        class ConstIterator;
        using value_type = T;
        using size_type = size_t;
        using difference_type = ptrdiff_t;
        using reference = value_type&;
        using const_reference = const value_type&;
        using pointer = value_type*;
        using const_pointer = const value_type*;
        using iterator = Vector::Iterator;
        using const_iterator = Vector::ConstIterator;

        Vector() {
                this->elems_max = INIT_SIZE;
                this->elems = new T[this->elems_max];
                this->elem_count = 0;
            }

        Vector(size_t size) {
                this->elems_max = size;
                if (size!=0) {
                    this->elems = new T[this->elems_max];
                } else {
                    //let's allocate 1 element to not confuse all the rest of the code...
                    this->elems = new T[1];
                }
                this->elem_count = 0;
        }

        /**
         * Copy Constructor.
         */
        Vector(const Vector& other){
                this->elems_max = other.elems_max;
                this->elems = new T[this->elems_max];
                this->elem_count = other.elem_count;
                for (unsigned long i = 0; i < other.elem_count; i++) {
                  this->elems[i] = other.elems[i];
                }
        }

        Vector(std::initializer_list<T> init){
            this->elems_max = init.size();
            this->elems = new T[this->elems_max];
            this->elem_count = 0;
            for (T elem : init){
                this->push_back(elem);
            }
        }

        /**
         * Copy assignment operator.
         */
        Vector<T>& operator=(const Vector<T>& other){
            if (this != &other) // not a self-assignment
            {
                if (elems_max != other.elems_max) // resource cannot be reused
                {
                    delete[] this->elems;
                    this->elems = new T[other.elems_max];
                }
                for (int i = 0; i < other.elem_count; i++) {
                  this->push_back(other.elems[i]);
                }
            }
            return *this;
        }

        size_t capacity(){
            return elems_max;
        }

        ~ Vector() {
            delete[] this->elems;
        }

        void push_back(T new_elem){
            if(this->elems_max == this->elem_count){
                // we have to grow first if we are full
                // instructions don't say anything about an exception to throw
                grow(INIT_SIZE);
            }
            this->elems[this->elem_count++] = new_elem;
        }

        T pop_back(){
            if(this->elem_count == 0){
                throw runtime_error("Can not pop element from empty Vector!");
            }
            return this->elems[this->elem_count-- - 1];
        }
        
        Iterator insert(ConstIterator iterator, const T& value){
            auto index = iterator.getPtr() - begin().getPtr();

            if(this->elem_count < index){
              index = elem_count;
            }

            if(this->elems_max == this->elem_count){
                // we have to grow first if we are full
                // instructions don't say anything about an exception to throw
                grow(INIT_SIZE);
            }
            for(int i = this->elem_count - 1; i >= index; i--){
                *(this->elems + i+1) = *(this->elems + i);
            }
            *(this->elems + index) = value;

            this->elem_count++;
            
            return Iterator(this->elems + static_cast<size_t>(index));
        }
        
        Iterator erase(ConstIterator iterator){
            auto index = iterator.getPtr() - begin().getPtr();
            for(int i = index; i < elem_count - 1; i++){
                this->elems[i] = this->elems[i+1];
            }
            this->elem_count--;
            return Iterator(this->elems + static_cast<size_t>(index));
        }

        void print(){
            for(uint i = 0; i < this->elems_max; i++){
                cout << "elem[" << i << "] = " << *(elems + i) << "\n";
            }
        }

        bool empty() const{
            return this->elem_count==0;
        }
        
        size_t size() const{
            return this->elem_count;
        }

        void clear(){
            this->elem_count = 0;
        }

        void reserve(size_t n){
            if(this->elems_max < n){
                grow(n - this->elems_max);
            }
        }

        void shrink_to_fit(){
          T* new_array = new T[this->elem_count];
          for (int i = 0; i < this->elem_count; i++) {
            new_array[i] = this->elems[i];
          }
          delete[] this->elems;
          this->elems = new_array;
          this->elems_max = this->elem_count;
        }

        T& operator[](size_t index){
            if(index < 0 || index > this->elem_count - 1){
              throw runtime_error("Can not access vector out of bounds!");
            }
            return elems[index];
        }

        const T& operator[](size_t index) const{
            if(index < 0 || index > this->elem_count - 1){
              throw runtime_error("Can not access vector out of bounds!");
            }
            return elems[index];
        }
        
        Iterator begin(){
          return Iterator(this->elems, this);
        }
        
        Iterator end(){
          return Iterator(this->elems + this->elem_count, this);
        }
        
        ConstIterator begin() const{
          return ConstIterator(this->elems, this);
        }
        
        ConstIterator end() const{
          return ConstIterator(this->elems + this->elem_count, this);
        }

        T* getElems() const{
          return this->elems;
        }
    
    class Iterator{
      public:
      
        using value_type = Vector::value_type;
        using reference = Vector::reference;
        using pointer = Vector::pointer;
        using difference_type = Vector::difference_type;
        using iterator_category = std::forward_iterator_tag;
        
        Iterator(){
          this->ptr = NULL;
          this->vector = NULL;
        }
        
        Iterator(T* ptr){
          this->ptr = static_cast<T*>(ptr);
          this->vector = NULL;
        }
        
        Iterator(void* ptr, Vector* vector){
          this->ptr = static_cast<T*>(ptr);
          this->vector = vector;
        }

        Iterator(std::initializer_list<T> init){
          Vector<int> vector = Vector<T>(init);
          this->ptr = vector.getElems();
          this->vector = &vector;
        }
        
        T& operator*() const{
          return *ptr;
        }
        
        T* operator->() const{
          return ptr;
        }
        
        bool operator==(const ConstIterator& other) const{
          return this->ptr == other.getPtr();
        }
        
        bool operator!=(const ConstIterator& other) const{
          return this->ptr != other.getPtr();
        }
        
        T* getPtr() const{
          return this->ptr;
        }
        
        Iterator& operator++(){
          if(vector->empty()){
            throw runtime_error("Called ++ on an Iterator whos Vector is empty!");
          }
        
          if(this->ptr - vector->elems >= vector->elem_count){
            throw runtime_error("Called ++ on the last element of the Vector's Iterator!");
          }
        
          (this->ptr)++;
          return *this;
        }
        
        Iterator operator++(int){
          if(vector->empty()){
            throw runtime_error("Called ++ on an Iterator whos Vector is empty!");
          }
        
          if(this->ptr - vector->elems >= vector->elem_count){
            throw runtime_error("Called ++ on the last element of the Vector's Iterator!");
          }
        
          Iterator old(*this);
          (this->ptr)++;
          return std::move(old);
        }
        
        operator ConstIterator() const{
          return ConstIterator(this->ptr, this->vector);
        }
        
      private:
        Vector* vector;
        T* ptr;
    };
    
    class ConstIterator{
      public:
      
        using value_type = Vector::value_type;
        using reference = Vector::const_reference;
        using pointer = Vector::const_pointer;
        using difference_type = Vector::difference_type;
        using iterator_category = std::forward_iterator_tag;
        
        ConstIterator(){
          this->ptr = NULL;
          this->vector = NULL;
        }
        
        ConstIterator(T* ptr){
          this->ptr = static_cast<T*>(ptr);
          this->vector = NULL;
        }
        
        ConstIterator(T* ptr, const Vector* vector){
          this->ptr = ptr;
          this->vector = vector;
        }

        ConstIterator(std::initializer_list<T> init){
          const Vector<int> vector = Vector<T>(init);
          this->ptr = vector.getElems();
          this->vector = &vector;
        }
        
        T& operator*() const{
          return *ptr;
        }
        
        T* operator->() const{
          return ptr;
        }
        
        bool operator==(const ConstIterator& other) const{
          return this->ptr == other.getPtr();
        }
        
        bool operator!=(const ConstIterator& other) const{
          return this->ptr != other.getPtr();
        }
        
        T* getPtr() const{
          return this->ptr;
        }
        
        ConstIterator& operator++(){
          if(vector->empty()){
            throw runtime_error("Called ++ on an Iterator whos Vector is empty!");
          }
        
          if(static_cast<size_t>(this->ptr - vector->elems) >= vector->elem_count){
            throw runtime_error("Called ++ on the last element of the Vector's Iterator!");
          }
        
          (this->ptr)++;
          return *this;
        }
        
        ConstIterator operator++(int){
          if(vector->empty()){
            throw runtime_error("Called ++ on an Iterator whos Vector is empty!");
          }
        
          if(this->ptr - vector->elems >= vector->elem_count){
            throw runtime_error("Called ++ on the last element of the Vector's Iterator!");
          }
        
          ConstIterator old(*this);
          (this->ptr)++;
          return std::move(old);
        }
        
      private:
        T* ptr;
        const Vector* vector;
    };
    
        private:
        size_t elems_max;
        T* elems;
        size_t elem_count;

        void grow(size_t grow_by) {
          T* new_array = new T[this->elems_max + grow_by];
          for (unsigned long i = 0; i < this->elem_count; i++) {
            new_array[i] = this->elems[i];
          }
          delete[] this->elems;
          this->elems = new_array;
          this->elems_max += grow_by;
        }

    friend Vector<T> & operator<<(ostream& os, const Vector<T>& vector){

        os << "[";

        for (size_t i=0; i < vector.elems_max; i++){
            os << vector[i] << ", ";
        }

        // last element without ", ", except it's empty
        if(vector.elems_max != 0){
            os << vector[vector.elems_max - 1];
        }

        os << "]";

        return os;
    };
};

#endif //VECTOR_H
