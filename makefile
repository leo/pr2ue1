TARGET_EXEC := hello_world

CC=clang
CXX=clang++
CXXFLAGS=-g -Og -Wall -Wextra --pedantic-errors --std=c++17

BUILD_DIR := ./build
TEST_BUILD_DIR := $BUILD_DIR/test
SRC_DIRS := ./src
TEST_DIRS := ./test

# Find all the C and C++ files we want to compile
# Note the single quotes around the * expressions. The shell will incorrectly expand these otherwise, but we want to send the * directly to the find command.
SRCS := $(shell find $(SRC_DIRS) -name '*.cpp' -or -name '*.c' -or -name '*.s')

TESTS := $(ptsubst %.cpp,%,$(shell find $(TEST_DIRS) -name '*.cpp' -or -name '*.c' -or -name '*.s'))

# Prepends BUILD_DIR and appends .o to every src file
# As an example, ./your_dir/hello.cpp turns into ./build/./your_dir/hello.cpp.o
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)

# String substitution (suffix version without %).
# As an example, ./build/hello.cpp.o turns into ./build/hello.cpp.d
DEPS := $(OBJS:.o=.d)

# Every folder in ./src will need to be passed to GCC so that it can find header files
INC_DIRS := $(shell find $(SRC_DIRS) -type d)
# Add a prefix to INC_DIRS. So moduleA would become -ImoduleA. GCC understands this -I flag
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

# The -MMD and -MP flags together generate Makefiles for us!
# These files will have .d instead of .o as the output.
CPPFLAGS := $(INC_FLAGS) -MMD -MP

all: $(BUILD_DIR)/$(TARGET_EXEC)

# The final build step.
$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CXX) $(OBJS) -o $@ $(LDFLAGS)

# Build step for C++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

run: all
	./$(BUILD_DIR)/$(TARGET_EXEC)

test: $(TEST_BUILD_DIR)/$TESTS
	./$(TEST_BUILD_DIR)/$TESTS

utest_secure_iterators: $(TEST_DIRS)/%.cpp
	./$(BUILD_DIR)/test/$@

$(TEST_BUILD_DIR)/$TESTS: $(OBJS)
	$(CXX) $(OBJS) -o $@ $(LDFLAGS)

# Build step for C++ source
$(TEST_DIRS)/%.cpp.o: %.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

test1:
	g++ -std=c++11 -o build/test1 test/utest.cpp

test2:
	g++ -std=c++11 -o build/test2 test/utest_2.cpp

test_const:
	g++ -std=c++11 -o build/test_const test/const_test.cpp

test_const_arrow:
	g++ -std=c++11 -o build/test_const_arrow test/const_test_arrow.cpp

test_types:
	g++ -std=c++11 -o build/test_types test/types_test.cpp

test_secure_iterators:
	g++ -std=c++11 -o build/test_secure_iterators test/utest_secure_iterators.cpp


.PHONY: clean
clean:
	rm -r $(BUILD_DIR)

# Include the .d makefiles. The - at the front suppresses the errors of missing
# Makefiles. Initially, all the .d files will be missing, and we don't want those
# errors to show up.
-include $(DEPS)
